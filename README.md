[![Build Status](https://gitlab.com/dnb_cs_capstone/healytics_capstone/badges/master/pipeline.svg)](https://gitlab.com/dnb_cs_capstone/healytics_capstone/-/commits/master)

## Foundation Ingestion QA Capabilities

This is a senior capstone project for @damorales9 and @littlepaz

# Overview of the Project

This project is for Healytics Inc. and will be a Java Maven project working with PostgreSQL to implement a software for data validation and lookup for
human genetic data. We will be provided lab reports that are part of their already predefined PDF injestion process which will then give us the data
pulled from said PDF lab reports and then be handed off to us for validation. This validation includes the verifying of Genomic Findings and Biomarker 
Findings. As well as the verfication of proper general demographic information. We are to compare and validate data given to us against the data found
on human genetics from Gencode. This information from gencode will be pulled and refreshed daily in order to assure a valid and correct data validation 
process.

# Implemented Features

1. We have the Kron Job implemented to properly retrieve the human gene information from Gencode and then store that data into a PostregeSQL database to then be used later.
2. We have implemented the process of taking the JSON files produced after their PDF ingestion process with all the genetic data included in it and parsing the file in order to make the proper JSON objects needed to do the validation.
3. We have implemented a basic validation process which validates the data and will return a score for how accurate the data is.

# Not Yet Implemented Features

1. We still need to have the software return the pdf with the incorrect data as a flagged JSON back to the user for human intervention.
2. We still need to come up with a more valued validation system based on each data point having a certain weight.
3. We need to include different versions of human genetic information and validate against those as well.

# Dependencies

1. PostgreSQL
2. Jackson
3. JOOQ
4. JUNIT Jupiter
5. BIOJAVA
6. Maven Plugins

# Instructions for Compilation and Running

1. Clone the repo
1. Download and Install Maven
2. Download and Install PostgreSQL
3. Open pgAdmin and PostgreSQL
4. Run SQL queries provided
    1. CREATE TABLE public."GenomicCH38"
        (
            name character varying NOT NULL,
            start_position integer,
            end_position integer,
            PRIMARY KEY (name)
        );

        ALTER TABLE public."GenomicCH38"
            OWNER to postgres;
    2. CREATE TABLE public."GenomicCH38"
        (
            name character varying NOT NULL,
            start_position integer,
            end_position integer,
            PRIMARY KEY (name)
        );

        ALTER TABLE public."GenomicCH38"
            OWNER to postgres;
5. Install the maven project by running "mvn install" from inside the project folder
6. Update the database using the jar file with the command "java -jar foundation-ingestion-qa-capabilities-1.0-SNAPSHOT.jar -u"
7. run the software with the command "java -jar foundation-ingestion-qa-capabilities-1.0-SNAPSHOT.jar"
