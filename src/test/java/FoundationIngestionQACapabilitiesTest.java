import com.fasterxml.jackson.databind.ObjectMapper;
import findings.PDFResult;
import org.jooq.DSLContext;
import org.junit.Ignore;
import org.junit.Test;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

public class FoundationIngestionQACapabilitiesTest {

    PropertiesReader properties;

    public FoundationIngestionQACapabilitiesTest() throws IOException {
        properties = new PropertiesReader("project.properties");
    }

    @Test
    public void driverFileCheck() {
        String biomarkerPath = properties.getProperty("biomarker.results.path");
        String genomicPath = properties.getProperty("genomic.results.path");
        String singleResultPath = properties.getProperty("single.result.path");
        assert(new File(biomarkerPath).exists());
        assert(new File(genomicPath).exists());
        assert(new File(singleResultPath).exists());
    }

    @Test
    @Ignore
    public void validatePDFObjectMapping() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        PDFResult result = objectMapper.readValue(new File(properties.getProperty("single.result.path")), PDFResult.class);

        // DEMOGRAPHIC DATA
        assert result.getDemographicData().getDemographics().get("patientID").equals("MRN1234");
        assert result.getDemographicData().getDemographics().get("dob").equals("10/11/1944");
        assert result.getDemographicData().getDemographics().get("sex").equals("Male");
        assert result.getDemographicData().getDemographics().get("name").equals("Last, First");
        assert result.getDemographicData().getDemographics().get("age").equals("75 Y");
        assert result.getDemographicData().getDemographics().get("SSN") == null;

        // ORDER INFORMATION
        assert result.getOrderData().getOrders().get("copyFor").equals("");
        assert result.getOrderData().getOrders().get("study").equals("S01-1234-03");
        assert result.getOrderData().getOrders().get("order_number").equals("O1234");
        assert result.getOrderData().getOrders().get("received").equals("7/17/2020 8:33 AM");
        assert result.getOrderData().getOrders().get("billingNumber").equals("1313474716");
        assert result.getOrderData().getOrders().get("collected").equals("7/17/2020 8:00 AM");
        assert result.getOrderData().getOrders().get("location").equals("LOC1");

        // CLINICAL FINDINGS
        assert result.getResultData().getResults().getClinical().size() == 5;
        assert result.getResultData().getResults().getClinical().get(0).getResult().gene.equals("ALK");
        assert result.getResultData().getResults().getClinical().get(0).getResult().pctVAF == 44.03;
        assert result.getResultData().getResults().getClinical().get(0).getResult().databaseID.equals("rs371652715");
        assert result.getResultData().getResults().getClinical().get(0).getResult().chromosome.equals("chr2");
        assert result.getResultData().getResults().getClinical().get(0).getResult().position == 29606601;
        assert result.getResultData().getResults().getClinical().get(0).getResult().aaChange.equals("p.E427K");
        assert result.getResultData().getResults().getClinical().get(0).getResult().notes.equals("");
        assert result.getResultData().getResults().getClinical().get(0).getResult().baseChange.equals("C>T");

        // NON CLINICAL FINDINGS
        assert result.getResultData().getResults().getOther().size() == 6;
        assert result.getResultData().getResults().getOther().get(0).getResult().get("Gene").equals("CPS1");
        assert result.getResultData().getResults().getOther().get(0).getResult().get("%VAF").equals("7.95");
        assert result.getResultData().getResults().getOther().get(0).getResult().get("Reference Database ID").equals("COSM8611783");
        assert result.getResultData().getResults().getOther().get(0).getResult().get("Chr:Pos").equals("chr2:211465419");
        assert result.getResultData().getResults().getOther().get(0).getResult().get("AA_change").equals("p.S564R");
        assert result.getResultData().getResults().getOther().get(0).getResult().get("Notes").equals("");
        assert result.getResultData().getResults().getOther().get(0).getResult().get("Base Change").equals("A>C");

        // GENOMIC
        assert result.getResultData().getResults().getGenomicFindings().get(0).getResult().size() == 13;
        assert result.getResultData().getResults().getGenomicFindings().get(0).getResult().get("ALK").equals("p.E427K");
        assert result.getResultData().getResults().getGenomicFindings().get(0).getResult().get("SMAD4").equals("p.S210fs");
        assert result.getResultData().getResults().getGenomicFindings().get(0).getResult().get("CDKN2A").equals("p.R58*");
        assert result.getResultData().getResults().getGenomicFindings().get(0).getResult().get("FANCL").equals("p.T367fs");
        assert result.getResultData().getResults().getGenomicFindings().get(0).getResult().get("TET2").equals("p.N275fs");
        assert result.getResultData().getResults().getGenomicFindings().get(0).getResult().get("FGF3").equals("Amplified");
        assert result.getResultData().getResults().getGenomicFindings().get(0).getResult().get("RECQL4").equals("p.R754W");
        assert result.getResultData().getResults().getGenomicFindings().get(0).getResult().get("RAD51").equals("p.G307R");
        assert result.getResultData().getResults().getGenomicFindings().get(0).getResult().get("CPS1").equals("p.S564R");
        assert result.getResultData().getResults().getGenomicFindings().get(0).getResult().get("CUX1").equals("p.V99I");
        assert result.getResultData().getResults().getGenomicFindings().get(0).getResult().get("CCND1").equals("Amplified");
        assert result.getResultData().getResults().getGenomicFindings().get(0).getResult().get("KRAS").equals("p.G12D");
        assert result.getResultData().getResults().getGenomicFindings().get(0).getResult().get("TP53").equals("p.H178fs");

        // BIOMARKER
        assert result.getResultData().getResults().getBioMarkerFindings().get(0).getResult().get("Tumor Mutational Burden").equals("2.64");

        // COPY
        assert result.getResultData().getResults().getCopyNumberVariant().get(0).getResult().get("Gene").equals("CCND1");
        assert result.getResultData().getResults().getCopyNumberVariant().get(0).getResult().get("Copy Number Variant").equals("Amplified");
        assert result.getResultData().getResults().getCopyNumberVariant().get(0).getResult().get("Chromosome").equals("chr11");
        assert result.getResultData().getResults().getCopyNumberVariant().get(0).getResult().get("Notes").equals("");

        // METADATA
        assert result.getReportMetadata().getMetadata().get("sha256sum").equals("B0E2FB089745FD523EA374ED5780B3FCDB7AF2C71E522667BB162B70298C8373");
        assert result.getReportMetadata().getMetadata().get("processDateTime").equals("1615403666837");
        assert result.getReportMetadata().getMetadata().get("storageUrl").equals("/api/genetic_query_builder/pdf_result/MRN1234");
    }

    @Test
    @Ignore
    public void validatePDFResults() throws IOException, SQLException {
        FoundationIngestionQACapabilities qac = new FoundationIngestionQACapabilities();
        String resultPath = properties.getProperty("single.result.path");
        assert(qac.validateResult(resultPath) > .9);
    }

}