import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;
import java.net.URL;
import java.util.logging.*;
import java.util.zip.GZIPInputStream;

import findings.BioMarkerFinding;
import findings.GenomicFinding;
import findings.PDFResult;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import static database.Tables.GENOMICCH37;
import static database.Tables.GENOMICCH38;


/**
 * Validate genomic and biomarker data for patients
 *
 * @author Ben Pazienza
 * @author Derek Morales
 * @version 1.0
 */
public class FoundationIngestionQACapabilities {

    /**
     * Logging the output
     */
    private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    /**
     * The default buffer size to use while reading files through input streams
     */
    private static final int BUFFER_SIZE = 1024;

    /**
     * The path ot the GENCODE data for the GRCH38 genome assembly version
     */
    private static final String FILE_URL_CH38 = "ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_37/gencode.v37.annotation.gff3.gz";

    /**
     * The path ot the GENCODE data for the GRCH37 genome assembly version
     */
    private static final String FILE_URL_CH37 = "ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_19/gencode.v19.annotation.gff3.gz";

    /**
     * Index of the chromosome number in a single line of the GRCH38 GENCODE data
     * From GENCODE: chr{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y,M} or GRC accession
     */
    private static final int CH38_CHROMOSOME_INDEX = 0;

    /**
     * Index of the feature type of a single line of the GRCH38 GENCODE data
     * From GENCODE: {gene,transcript,exon,CDS,UTR,start_codon,stop_codon,Selenocysteine}
     */
    private static final int CH38_GENE_INDEX = 2;

    /**
     * Index of the genomic start position of a single line of the GRCH38 GENCODE data
     * From GENCODE: integer-value (1-based)
     */
    private static final int CH38_GENOMIC_START_POS_INDEX = 3;

    /**
     * Index of the genomic end position of a single line of the GRCH38 GENCODE data
     * From GENCODE: integer-value
     */
    private static final int CH38_GENOMIC_END_POS_INDEX = 4;

    /**
     * Index of the genomic phase data of a single line of the GRCH38 GENCODE data. This includes
     * values of the gene name, gene type, and several other data outside of the scope of our product
     * From GENCODE: {0, 1, 2, .}
     */
    private static final int CH38_GENE_ARRAY_INDEX = 8;

    /**
     * Index of the gene name inside the genomic phase array of a single line of the GRCH38 GENCODE data
     * It follows the syntax: gene_name=[name]
     */
    private static final int CH38_GENE_ARRAY_GENE_NAME_INDEX = 3;


    /**
     * Index of the chromosome number in a single line of the GRCH37 GENCODE data
     * From GENCODE: chr{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y,M} or GRC accession
     */
    private static final int CH37_CHROMOSOME_INDEX = 0;

    /**
     * Index of the feature type of a single line of the GRCH37 GENCODE data
     * From GENCODE: {gene,transcript,exon,CDS,UTR,start_codon,stop_codon,Selenocysteine}
     */
    private static final int CH37_GENE_INDEX = 2;

    /**
     * Index of the genomic start position of a single line of the GRCH37 GENCODE data
     * From GENCODE: integer-value (1-based)
     */
    private static final int CH37_GENOMIC_START_POS_INDEX = 3;

    /**
     * Index of the genomic end position of a single line of the GRCH37 GENCODE data
     * From GENCODE: integer-value
     */
    private static final int CH37_GENOMIC_END_POS_INDEX = 4;

    /**
     * Index of the genomic phase data of a single line of the GRCH37 GENCODE data. This includes
     * values of the gene name, gene type, and several other data outside of the scope of our product
     * From GENCODE: {0, 1, 2, .}
     */
    private static final int CH37_GENE_ARRAY_INDEX = 8;

    /**
     * Index of the gene name inside the genomic phase array of a single line of the GRCH37 GENCODE data
     * It follows the syntax: gene_name=[name]
     */
    private static final int CH37_GENE_ARRAY_GENE_NAME_INDEX = 5;

    /**
     * The properties parsed form the pom.xml At run time they get parsed from the pom and stored in a
     * file under the build directory specified also in the pom.xml
     */
    private static PropertiesReader properties;

    /**
     * A context object for direct connections to the PostgreSQL database. Via this object the
     * {@link database.tables.Genomicch37#GENOMICCH37} and {@link database.tables.Genomicch38#GENOMICCH38}
     * tables can be accessed and mutated
     */
    private static DSLContext create = getDBConnection();

    /**
     * Default Constructor
     */
    public FoundationIngestionQACapabilities() {
    }

    /**
     * Validate the JSON file found at resultPath
     * using the {@link GenomicFinding#validateCH37(String, DSLContext)} and
     * {@link findings.BioMarkerFinding#validate(DSLContext)} functions
     *
     * @param resultPath the path for the JSON file
     * @return a floating number representing the percent of the data that was valid
     * @throws IOException if the file path is bad
     */
    public double validateResult(String resultPath) throws IOException {

        PDFResult pdfResult = new ObjectMapper().readValue(new File(resultPath), PDFResult.class);

        List<Double> validationResultsCH38 = new ArrayList<>();
        List<Double> validationResultsCH37 = new ArrayList<>();
        List<Double> validationResultsBIO = new ArrayList<>();
        try{
            pdfResult.getResultData().getResults().getBioMarkerFindings().forEach(finding -> {
                double result = 0;
                try {
                    result = finding.validate(create);
                } catch (IOException e) {
                    LOGGER.severe("ERROR: BIOMARKER CSV FILE NOT FOUND");
                    e.printStackTrace();
                }
                validationResultsBIO.add(result);
                if (result != 1) {
                    LOGGER.severe(String.format("Validating BioMarkers for object %s got result %f", finding, result));
                }
            });
        }
        catch (NullPointerException exception){
            exception.printStackTrace();
            LOGGER.severe("WARNING: PDF IS MISSING BIOMARKER FINDINGS.");
        }

        try{
            pdfResult.getResultData().getResults().getClinical().forEach(finding -> {
                double result = finding.validateCH38(create);
                validationResultsCH38.add(result);
                if (result != 1) {
                    LOGGER.severe(String.format("Validating against GRCH38 for object %s got result %f", finding.toStringPrecise(), result));
                }
                result = finding.validateCH37(create);
                validationResultsCH37.add(result);
                if (result != 1) {
                    LOGGER.severe(String.format("Validating against GRCH37 for object %s got result %f", finding.toStringPrecise(), result));
                }
            });
        }
        catch (NullPointerException exception){
            LOGGER.severe("WARNING: PDF IS MISSING CLINICAL FINDINGS.");
        }


        double validationResultsDEMO = pdfResult.getDemographicData().validate();

        System.out.printf("\n\nOverall PDF Validation Results for \n%s\n", pdfResult.getReportMetadata());
        System.out.printf("----------------------------------------------------------------------------------------\n");
        System.out.printf("Average clinical result validation against CH38 was %s\n", average(validationResultsCH38));
        System.out.printf("Average clinical result validation against CH37 was %s\n", average(validationResultsCH37));
        System.out.printf("BioMarker validation was %s\n", average(validationResultsBIO));
        System.out.printf("Demographic validation was %f\n", validationResultsDEMO);

        return average(validationResultsCH38);
    }

    /**
     * Decompress a .gz file from the path source and write it to target
     *
     * @param source the path where the .gz is located
     * @param target the location to write its contents to
     * @throws IOException
     */
    public static void decompressGzip(Path source, Path target) {

        try (GZIPInputStream gis = new GZIPInputStream(
                new FileInputStream(source.toFile()));
             FileOutputStream fos = new FileOutputStream(target.toFile())) {
            System.out.println("Decompressing");
            // copy GZIPInputStream to FileOutputStream
            byte[] buffer = new byte[BUFFER_SIZE];
            int len;
            while ((len = gis.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }

        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.severe(e.getLocalizedMessage());
            System.exit(1);
        }

    }

    /**
     * Get the average from a list of doubles
     *
     * @param doubles the list of doubles to average
     * @return the average of the doubles list
     */
    public double average(List<Double> doubles) {
        double sum = 0;
        for (double i : doubles) {
            sum += i;
        }
        return sum / doubles.size();
    }

    /**
     * Download the GRCH38 assembly version of the GENCODE database, unzip it, and then
     * parse it into the PostgreSQL database
     *
     * @see database.tables.Genomicch38
     */
    public static void downloadCH38GencodeFile() {
        String FILE_NAME = properties.getProperty("gencodegzch38.results.path");
        String UNZIP_FILE_NAME = properties.getProperty("gencodech38.results.path");

        downloadDecompress(FILE_NAME, UNZIP_FILE_NAME, FILE_URL_CH38);


        try {
            BufferedReader in = new BufferedReader(new FileReader(new File(UNZIP_FILE_NAME)));
            String line;
            while ((line = in.readLine()) != null) {
                if (line.charAt(0) == '#') {
                    continue;
                }
                String[] ch38Arr = line.split("\t");
                if (ch38Arr.length > CH38_GENE_ARRAY_INDEX && ch38Arr[CH38_GENE_INDEX].equals("gene")) {
                    String[] geneArr = ch38Arr[CH38_GENE_ARRAY_INDEX].split(";");
                    int startPos, endPos;
                    try {
                        startPos = Integer.parseInt(ch38Arr[CH38_GENOMIC_START_POS_INDEX]);
                        endPos = Integer.parseInt(ch38Arr[CH38_GENOMIC_END_POS_INDEX]);
                        if (startPos > endPos) {
                            throw new NumberFormatException("Start must be lower than end");
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                        continue;
                    }
                    if (geneArr.length > CH38_GENE_ARRAY_GENE_NAME_INDEX) {
                        String[] geneNameArr = geneArr[CH38_GENE_ARRAY_GENE_NAME_INDEX].split("=");
                        if (geneNameArr.length == 2) {
                            String geneNameKey = String.format("%s:%s", geneNameArr[1], ch38Arr[CH38_CHROMOSOME_INDEX]);
                            if (create.selectFrom(GENOMICCH38).where(GENOMICCH38.NAME.eq(geneNameKey)).fetchOne() == null) {
                                create.insertInto(GENOMICCH38, GENOMICCH38.NAME, GENOMICCH38.START_POSITION, GENOMICCH38.END_POSITION)
                                        .values(geneNameKey, startPos, endPos)
                                        .execute();
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Download the GRCH37 assembly version of the GENCODE database, unzip it, and then
     * parse it into the PostgreSQL database
     *
     * @see database.tables.Genomicch37
     */
    public static void downloadCH37GencodeFile() {
        String FILE_NAME = properties.getProperty("gencodegzch37.results.path");
        String UNZIP_FILE_NAME = properties.getProperty("gencodech37.results.path");

        downloadDecompress(FILE_NAME, UNZIP_FILE_NAME, FILE_URL_CH37);

        try {
            BufferedReader in = new BufferedReader(new FileReader(new File(UNZIP_FILE_NAME)));
            String line;
            while ((line = in.readLine()) != null) {
                if (line.charAt(0) == '#') {
                    continue;
                }
                String[] ch37Arr = line.split("\t");
                if (ch37Arr.length > CH37_GENE_ARRAY_INDEX && ch37Arr[CH37_GENE_INDEX].equals("gene")) {
                    String[] geneArr = ch37Arr[CH37_GENE_ARRAY_INDEX].split(";");
                    int startPos, endPos;
                    try {
                        startPos = Integer.parseInt(ch37Arr[CH37_GENOMIC_START_POS_INDEX]);
                        endPos = Integer.parseInt(ch37Arr[CH37_GENOMIC_END_POS_INDEX]);
                        if (startPos > endPos) {
                            throw new NumberFormatException("Start must be lower than end");
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                        continue;
                    }
                    if (geneArr.length > CH37_GENE_ARRAY_GENE_NAME_INDEX) {
                        String[] geneNameArr = geneArr[CH37_GENE_ARRAY_GENE_NAME_INDEX].split("=");
                        if (geneNameArr.length == 2) {
                            String geneNameKey = String.format("%s:%s", geneNameArr[1], ch37Arr[CH37_CHROMOSOME_INDEX]);
                            if (create.selectFrom(GENOMICCH37).where(GENOMICCH37.NAME.eq(geneNameKey)).fetchOne() == null) {
                                create.insertInto(GENOMICCH37, GENOMICCH37.NAME, GENOMICCH37.START_POSITION, GENOMICCH37.END_POSITION)
                                        .values(geneNameKey, startPos, endPos)
                                        .execute();
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Download a gz compressed GFF file from the internet, decompress it and save it to file
     *
     * @param fileName the path to save the gz file to
     * @param unzipFileName the path to save the gff file
     * @param url the internet path to the gz file
     */
    private static void downloadDecompress(String fileName, String unzipFileName, String url) {
        Path gz = Paths.get(fileName);
        Path gff = Paths.get(unzipFileName);

        if (!Files.exists(gff)) {
            try (BufferedInputStream in = new BufferedInputStream(new URL(url).openStream());
                 FileOutputStream fileOutputStream = new FileOutputStream(fileName)) {

                byte[] dataBuffer = new byte[BUFFER_SIZE];
                int bytesRead;
                while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                    fileOutputStream.write(dataBuffer, 0, bytesRead);
                }
            } catch (IOException e) {
                // handle exception
                e.printStackTrace();
            }
            decompressGzip(gz, gff);
        }
    }

    /**
     * Main method initializes a run of the validation software from given JSON data in
     * given json file.
     *
     * @param args Can contain the -u flag to trigger the software to update the database
     *             with the latest gencode data on gene names and gene variants
     */
    public static void main(String[] args) {

        // Initialize project property reader for variables defined in pom.xml
        try {
            properties = new PropertiesReader("project.properties");
            FileHandler fh = new FileHandler("fiQAc.log");
            fh.setLevel(Level.ALL);
            LOGGER.setLevel(Level.SEVERE);
            LOGGER.addHandler(fh);
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.severe(e.getLocalizedMessage());
            System.exit(1);
        }

        // Update database from gencode if -u flag is provided
        if (args.length == 1 && args[0].equals("-u")) {
            downloadCH37GencodeFile();
            downloadCH38GencodeFile();
        } else {
            try {
                BioMarkerFinding.generateValidBiomarkers(properties.getProperty("biomarker.results.path"));
                FoundationIngestionQACapabilities qac = new FoundationIngestionQACapabilities();
                qac.validateResult(properties.getProperty("single.result.path"));
            } catch (IOException e) {
                e.printStackTrace();
                LOGGER.severe(e.getLocalizedMessage());
            }
        }
    }

    public static DSLContext getDBConnection() {
        String userName = "postgres";
        String password = "admin";
        String url = "jdbc:postgresql://127.0.0.1/postgres";

        try {
            return DSL.using(DriverManager.getConnection(url, userName, password), SQLDialect.POSTGRES);
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return null;
    }
}
