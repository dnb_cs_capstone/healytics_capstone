package findings;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class Result implements Serializable {

    List<ClinicalResult> clinical;
    List<OtherResult> other;
    List<GenomicFinding> genomicFindings;
    List<BioMarkerFinding> bioMarkerFindings;
    List<CopyNumberVariant> copyNumberVariant;

    @JsonCreator
    public Result(@JsonProperty("clinical") List<ClinicalResult> clinical,
                  @JsonProperty("other") List<OtherResult> other,
                  @JsonProperty("genomicFindings") List<GenomicFinding> genomicFindings,
                  @JsonProperty("bioMarkerFindings") List<BioMarkerFinding> bioMarkerFindings,
                  @JsonProperty("copyNumberVariant") List<CopyNumberVariant> copyNumberVariant) {
        this.clinical = clinical;
        this.other = other;
        this.genomicFindings = genomicFindings;
        this.bioMarkerFindings = bioMarkerFindings;
        this.copyNumberVariant = copyNumberVariant;
    }

    public List<ClinicalResult> getClinical() {
        return clinical;
    }

    public List<OtherResult> getOther() {
        return other;
    }

    public List<GenomicFinding> getGenomicFindings() {
        return genomicFindings;
    }

    public List<BioMarkerFinding> getBioMarkerFindings() {
        return bioMarkerFindings;
    }

    public List<CopyNumberVariant> getCopyNumberVariant() {
        return copyNumberVariant;
    }

    @Override
    public String toString() {
        return "Result{" +
                "clinical=" + clinical +
                ", other=" + other +
                ", genomicFindings=" + genomicFindings +
                ", bioMarkerFindings=" + bioMarkerFindings +
                ", copyNumberVariant=" + copyNumberVariant +
                '}';
    }
}
