package findings;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Map;

public class OrderData implements Serializable {

    private Map<String, Object> orders;

    @JsonCreator
    public OrderData(@JsonProperty("orders") Map<String ,Object> orders) {
        this.orders = orders;
    }

    public Map<String, Object> getOrders() {
        return orders;
    }

    @Override
    public String toString() {
        return "OrderData{" +
                "orders=" + orders +
                '}';
    }
}
