package findings;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Map;

public class ReportMetadata implements Serializable {

    Map<String, Object> metadata;

    @JsonCreator
    public ReportMetadata(@JsonProperty("metadata") Map<String, Object> metadata) {
        this.metadata = metadata;
    }

    public Map<String, Object> getMetadata() {
        return metadata;
    }

    @Override
    public String toString() {
        String meta = "Report Metadata:";
        for (String k : metadata.keySet()) {
            meta += String.format("\n%s - %s", k, metadata.get(k));
        }
        return meta;
    }
}
