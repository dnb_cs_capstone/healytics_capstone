package findings;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class PDFResult implements Serializable {

    private DemographicData demographicData;
    private OrderData orderData;
    private ResultData resultData;
    private ReportMetadata reportMetadata;

    @JsonCreator
    public PDFResult(@JsonProperty("demographicData") DemographicData demographicData,
                     @JsonProperty("orderData") OrderData orderData,
                     @JsonProperty("resultData") ResultData resultData,
                     @JsonProperty("reportMetadata") ReportMetadata reportMetadata) {
        this.demographicData = demographicData;
        this.orderData = orderData;
        this.resultData = resultData;
        this.reportMetadata = reportMetadata;
    }

    public DemographicData getDemographicData() {
        return demographicData;
    }

    public OrderData getOrderData() {
        return orderData;
    }

    public ResultData getResultData() {
        return resultData;
    }

    public ReportMetadata getReportMetadata() {
        return reportMetadata;
    }

    @Override
    public String toString() {
        return "PDFResult{" +
                "demographicData=" + demographicData +
                ", orderData=" + orderData +
                ", resultData=" + resultData +
                ", reportMetadata=" + reportMetadata +
                '}';
    }
}
