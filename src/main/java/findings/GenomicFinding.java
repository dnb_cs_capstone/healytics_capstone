package findings;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import database.tables.records.Genomicch38Record;
import database.tables.records.Genomicch37Record;
import org.jooq.DSLContext;

import java.util.Map;

import static database.Tables.GENOMICCH38;
import static database.Tables.GENOMICCH37;

public class GenomicFinding {

    Map<String, Object> result;

    @JsonCreator
    public GenomicFinding(@JsonProperty("result") Map<String, Object> result) {
        this.result = result;
    }

    public Map<String, Object> getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "GenomicFinding{" +
                "result=" + result +
                '}';
    }

    public double validateCH38(String key, DSLContext db) {
        Genomicch38Record record = db.selectFrom(GENOMICCH38).where(GENOMICCH38.NAME.eq(key)).fetchOne();
        if(record == null) {
            System.out.printf("GENE NAME: %s NOT FOUND IN DB\n", key);
            return 0;
        } else {
            return 1;
        }
    }

    public double validateCH37(String key, DSLContext db) {
        Genomicch37Record record = db.selectFrom(GENOMICCH37).where(GENOMICCH37.NAME.eq(key)).fetchOne();
        if(record == null) {
            System.out.printf("GENE NAME: %s NOT FOUND IN DB\n", key);
            return 0;
        } else {
            return 1;
        }
    }
}
