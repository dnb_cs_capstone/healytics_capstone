package findings;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import database.tables.records.Genomicch37Record;
import database.tables.records.Genomicch38Record;
import org.jooq.DSLContext;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import static database.Tables.GENOMICCH37;
import static database.Tables.GENOMICCH38;


public class ClinicalResult implements Serializable {

    final double GENE_WEIGHT = 0.4;
    final double POSITION_WEIGHT = 0.4;
    final double VAF_WEIGHT = 0.1;
    final double BASE_CHANGE_WEIGHT = 0.1;

    public final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    private ClinicalResultData result;

    @JsonCreator
    public ClinicalResult(@JsonProperty("result") ClinicalResultData result) {
        this.result = result;
    }

    public ClinicalResultData getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "ClinicalResult{" +
                "result=" + result +
                '}';
    }

    public String toStringPrecise() {
        return result.gene + ":" + result.chromosome + " (" + result.baseChange + ")";
    }

    public double validateCH38(DSLContext db) {
        Genomicch38Record record = db.selectFrom(GENOMICCH38).where(GENOMICCH38.NAME.eq(String.format("%s:%s", result.gene, result.chromosome))).fetchOne();
        if(record == null) {
            LOGGER.info(String.format("RESULT %s WAS NOT FOUND IN THE GRCH38 DATABASE\n", toStringPrecise()));
            return 0;
        } else {
            double confidence = GENE_WEIGHT;
            if(record.getStartPosition() > result.position || record.getEndPosition() < result.position) {
                LOGGER.severe(String.format("RESULT %s POSITION INVALID %d IS NOT IN THE GRCH38 RANGE %d-%d", toStringPrecise(), result.position, record.getStartPosition(),record.getEndPosition()));
            } else {
                confidence += POSITION_WEIGHT;
            }
            confidence += validateBaseAndVAF();

            return confidence;
        }
    }

    public double validateCH37(DSLContext db) {
        Genomicch37Record record = db.selectFrom(GENOMICCH37).where(GENOMICCH37.NAME.eq(String.format("%s:%s", result.gene, result.chromosome))).fetchOne();
        if(record == null) {
            LOGGER.severe(String.format("RESULT %s WAS NOT FOUND IN THE GRCH37 DATABASE\n", toStringPrecise()));
            return 0;
        } else {
            double confidence = GENE_WEIGHT;
            if(record.getStartPosition() > result.position || record.getEndPosition() < result.position) {
                LOGGER.severe(String.format("RESULT %s POSITION INVALID %d IS NOT IN THE GRCH37 RANGE %d-%d", toStringPrecise(), result.position, record.getStartPosition(),record.getEndPosition()));
            } else {
                confidence += POSITION_WEIGHT;
            }
            confidence += validateBaseAndVAF();

            return confidence;
        }
    }

    private double validateBaseAndVAF() {
        double confidence = 0;
        if(result.pctVAF < 0 || result.pctVAF > 100) {
            LOGGER.severe(String.format("RESULT %s PERCENT VAF %f IS INVALID", toStringPrecise(), result.pctVAF));
        } else {
            confidence += VAF_WEIGHT;
        }
        if(!result.validateBaseChange()) {
            LOGGER.severe(String.format("RESULT %s BASE CHANGE %s IS INVALID", toStringPrecise(), result.baseChange));
        } else {
            confidence += BASE_CHANGE_WEIGHT;
        }
        return confidence;
    }
}
