package findings;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Map;

public class ResultData implements Serializable {

    private Result results;
    private Map<String, Object> comments;

    @JsonCreator
    public ResultData(@JsonProperty("results") Result results,
                      @JsonProperty("comments") Map<String, Object> comments) {
        this.results = results;
        this.comments = comments;
    }

    public Result getResults() {
        return results;
    }

    public Map<String, Object> getComments() {
        return comments;
    }

    @Override
    public String toString() {
        return "ResultData{" +
                "results=" + results +
                ", comments=" + comments +
                '}';
    }
}
