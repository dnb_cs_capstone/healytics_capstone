package findings;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Map;

public class CopyNumberVariant implements Serializable {

    Map<String, Object> result;

    @JsonCreator
    public CopyNumberVariant(@JsonProperty("result") Map<String, Object> result) {
        this.result = result;
    }

    public Map<String, Object> getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "CopyNumberVariant{" +
                "result=" + result +
                '}';
    }
}
