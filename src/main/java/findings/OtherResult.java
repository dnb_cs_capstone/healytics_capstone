package findings;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Map;

public class OtherResult implements Serializable {

    Map<String, Object> result;

    @JsonCreator
    public OtherResult(@JsonProperty("result") Map<String, Object> result) {
        this.result = result;
    }

    public Map<String, Object> getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "OtherResult{" +
                "result=" + result +
                '}';
    }
}
