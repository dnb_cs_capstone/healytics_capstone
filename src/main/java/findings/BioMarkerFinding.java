package findings;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.jooq.DSLContext;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

public class BioMarkerFinding {

    Map<String, Object> result;

    public final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    /**
     * Initialize list of valid biomarkers
     */
    public static ArrayList<String> validBiomarkers;

    /**
     * Initialize a list of valid biomarker results
     */
    public static ArrayList<String> validBiomarkerResults;


    @JsonCreator
    public BioMarkerFinding(@JsonProperty("result") Map<String, Object> result) {
        this.result = result;
    }

    public Map<String, Object> getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "BioMarkerFinding{" +
                "result=" + result +
                '}';
    }

    public static void generateValidBiomarkers(String filename) throws IOException {
        validBiomarkers = new ArrayList<>();
        validBiomarkerResults = new ArrayList<>();
        try{
            BufferedReader csvReader = new BufferedReader(new FileReader(filename));
            String row;
            csvReader.readLine();
            while ((row = csvReader.readLine()) != null) {
                String[] data = row.split(",");
                if (!validBiomarkers.contains(data[2])){
                    validBiomarkers.add(data[2]);
                }
                if(!data[2].equals("signedOutBy")){
                    if(!validBiomarkerResults.contains(data[3])){
                        validBiomarkerResults.add(data[3]);
                    }
                }
            }
            csvReader.close();
        }
        catch (FileNotFoundException exception){
            LOGGER.severe("BIOMARKER CSV FILE NOT FOUND");
            exception.printStackTrace();
        }
    }

    public double validate(DSLContext db) throws IOException {
        System.out.println(validBiomarkers.toString());
        System.out.println(validBiomarkerResults.toString());
        double confidence = 0.0;
        for (String key : result.keySet()) {
            if (!validBiomarkers.contains(key)) {
                confidence += 0;
            } else if (key.equals("signedOutBy")) {
                confidence += 0;
            } else if (key.equals("Tumor Mutational Burden")) {
                if (Double.parseDouble((String) result.get(key)) > 0.0 &&
                        Double.parseDouble((String) result.get(key)) < 10.0) {
                    confidence += 1.0 / result.size();
                }
            } else if (validBiomarkers.contains(key)) {
                if (validBiomarkerResults.contains(result.get(key))) {
                    confidence += 1.0 / result.size();
                }
            }
        }
        return confidence;
    }
}
