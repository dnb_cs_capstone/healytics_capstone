package findings;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class ClinicalResultData {

    final List<Character> VALID_BASE_CHANGE_CHARS = Arrays.asList('A', 'C', 'G', 'T', '<', '>', ' ');

    public String gene;
    public double pctVAF;
    public String databaseID;
    public String chromosome;
    public long position;
    public String aaChange;
    public String notes;
    public String baseChange;

    @JsonCreator
    public ClinicalResultData(@JsonProperty("Gene") String gene,
                              @JsonProperty("%VAF") double pctVAF,
                              @JsonProperty("Reference Database ID") String databaseID,
                              @JsonProperty("Chr:Pos") String chrPos,
                              @JsonProperty("Notes") String notes,
                              @JsonProperty("AA_change") String aaChange,
                              @JsonProperty("Base Change") String baseChange) {
        this.gene = gene;
        this.pctVAF = pctVAF;
        this.databaseID = databaseID;
        this.chromosome = chrPos.split(":")[0];
        this.position = Long.parseLong(chrPos.split(":")[1]);
        this.notes = notes;
        this.aaChange = aaChange;
        this.baseChange = baseChange;
    }

    @Override
    public String toString() {
        return "ClinicalResultData{" +
                "gene='" + gene + '\'' +
                ", pctVAF=" + pctVAF +
                ", databaseID='" + databaseID + '\'' +
                ", chromosome='" + chromosome + '\'' +
                ", position=" + position +
                ", notes='" + notes + '\'' +
                ", change='" + baseChange + '\'' +
                '}';
    }

    public boolean validateBaseChange() {
        for(char s: baseChange.toCharArray()) {
            if(!VALID_BASE_CHANGE_CHARS.contains(s)) {
                return false;
            }
        }
        return true;
    }
}

