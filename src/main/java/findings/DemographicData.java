package findings;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.logging.Logger;

public class DemographicData implements Serializable {

    public final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private static final double DEMOGRAPHIC_WEIGHT = 1.0 / 3.0;

    private Map<String, Object> demographics;

    @JsonCreator
    public DemographicData(@JsonProperty("demographics") Map<String, Object> demographics) {
        this.demographics = demographics;
    }

    public Map<String, Object> getDemographics() {
        return demographics;
    }

    public double validate() {
        double validation = 0;
        if(demographics.containsKey("age") && demographics.containsKey("dob")) {
            try {
                int age = Integer.parseInt(((String) demographics.get("age")).split(" ")[0]);
                if(age < 100 && age > 0) {
                    validation += DEMOGRAPHIC_WEIGHT;
                } else {
                    LOGGER.severe(String.format("DEMOGRAPHICS: Age %s is not a valid age", demographics.get("age")));
                }
            } catch (NumberFormatException n) {
                n.printStackTrace();
                LOGGER.severe(String.format("DEMOGRAPHICS: Could not parse %s to an integer for the age", demographics.get("age")));
            }

        }
        if(demographics.containsKey("dob")) {
            try {
                Calendar dob = Calendar.getInstance();
                Calendar now = Calendar.getInstance();
                dob.setTime(new SimpleDateFormat("MM/dd/yyyy").parse((String) demographics.get("dob")));
                if (dob.before(now) || dob.equals(now)) {
                    validation += DEMOGRAPHIC_WEIGHT;
                } else {
                    LOGGER.severe(String.format("DEMOGRAPHICS: Date of birth %s is in the future", demographics.get("dob")));
                }
            } catch (ParseException e) {
                e.printStackTrace();
                LOGGER.severe(String.format("DEMOGRAPHICS: Could not parse %s to a date for the dob", demographics.get("dob")));
            }
        }
        if(demographics.containsKey("sex")) {
            String sex = (String) demographics.get("sex");
            if(sex.equals("Male") || sex.equals("Female")) {
                validation += DEMOGRAPHIC_WEIGHT;
            }
        }
        return validation;
    }

    @Override
    public String toString() {
        return "DemographicData{" +
                "demographics=" + demographics +
                '}';
    }
}
